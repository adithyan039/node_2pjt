FROM node:15 as server

EXPOSE 5000

RUN npm i npm@latest -g


WORKDIR /app

COPY ["package.json", "package-lock.json*", "./"]

RUN npm install --production

COPY . .

CMD [ "node", "server.js" ]

#step2

FROM postgres:12 as database

 



COPY database.sql /docker-entrypoint-initdb.d/


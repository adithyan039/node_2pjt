const express = require('express');
const app = express();
const pool = require('./db');

app.use(express.json());
//get all
app.get('/todos/get',async(req,res)=>{
    try{
        const alltodo = await pool.query('SELECT * FROM todo');
        res.json(alltodo.rows);
    }catch(err){
        console.log(err);
    }
})

//get one


//create a todo
app.post('/todos',async(req,res)=>{
    try{
        const{description} = req.body;
        const newtodo = await pool.query("INSERT INTO todo (description) VALUES ($1) RETURNING *",[description]);
        res.json(newtodo.rows);
    }catch(err){
        console.log(err.message);
    }
})

//update

app.put('/todos/:id',async (req,res)=>{
    try{
       const {id} = req.params;
       const{description} = req.body;
       const updatedtodo = await pool.query('UPDATE todo SET description = $1 WHERE todo_id = $2',[description,id]);
       res.json('todo is updated')
    }catch(err){
        console.log(err);
    }

});
//delete
app.delete('/todos/delete/:id',async (req,res)=>{
    try{
     const{id} = req.params;
     const deletetodo = await pool.query('DELETE FROM todo WHERE todo_id = $1',[id]);
     res.json('deleted')
    }catch(err){
        console.log('error');
    }
})


app.listen(process.env.EXTERNAL_PORT,()=>{
    console.log('server is listening');
})